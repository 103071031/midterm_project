function init() {
    var user_email = '';
    var friend_email = '';
    var user_id = '';
    var friend_id = '';
    firebase.auth().onAuthStateChanged(function (user) {
        //var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            document.getElementById("username").innerHTML = "<img src='img/notification.png'>&nbsp;"+user_email;
            user_id = user.uid;
            firebase.database().ref('users/' + user_id).set({
                email: user.email
            });

            var logout_btn = document.getElementById("logout-btn");
            logout_btn.addEventListener('click', function(){
                firebase.auth().signOut().then(function() {
                    alert("Logout Success!");
                    window.location.assign("index.html");
                  }).catch(function() {
                    alert("Logout error!");
                  });
            });

            friend_list = document.getElementById("friend_list");
            search_friend = document.getElementById("search_friend");
            addfriend_btn = document.getElementById("addcontact");
            current_friend = document.getElementById("friend_current");
            
            addfriend_btn.addEventListener("click", function(){
                var user_exist = false;
                var friend_exist = false;
                if (search_friend.value != "") {
                    
                    firebase.database().ref("users/").once('value')
                        .then(function (snapshot) {
                            snapshot.forEach(function(childSnapshot) {
                                
                                // key will be "Alan" the first time and "Jim" the second time
                                var childid = childSnapshot.key;
                                var childemail = childSnapshot.val();
                                console.log(childemail.email);
                                if(childemail.email == search_friend.value){
                                    console.log("user exists");
                                    user_exist = true;
                                    friend_id = childid;
                                    friend_email = childemail.email;
                                    
                                    search_friend.value = "";
                                }
                            })
                            if(user_exist){
                                firebase.database().ref("message_list/"+user_id).once('value')
                                .then(function(snapshot){
                                    snapshot.forEach(function(childSnapshot){
                                        var childid = childSnapshot.key;
                                        if(friend_id == childid){
                                            friend_exist = true;
                                        }
                                    });
                                });
                                if(!friend_exist){
                                    current_friend.innerHTML = friend_email;
                                    firebase.database().ref("message_list/"+user_id+"/"+friend_id).update({
                                        email: friend_email
                                    });
                                    firebase.database().ref("message_list/"+friend_id+"/"+user_id).update({
                                        email: user_email
                                    });
                                    var total_post = [];
                                    firebase.database().ref("users/"+friend_id).once('value')
                                    .then(function(snapshot){
                                        friend_email = snapshot.val().email;
                                        console.log(friend_id, friend_email);
                                        current_friend.innerHTML = friend_email;
                                        firebase.database().ref("message_list/"+user_id+"/"+friend_id).on('child_added', function(data) {
                                            var childData = data.val();
                                            var childkey = data.key;
                                            if(childkey != "email"){
                                                if(childData.email == user_email) total_post[total_post.length] = str_before_user + childData.data + str_after_user;
                                                else total_post[total_post.length] = str_before_friend + childData.data + str_after_friend;
                                                message_list.innerHTML = total_post.join("");
                                            }
                                        })
                                        .catch(e =>{ console.log(e.message)});
                                    });
                                }
                            }
                            else{
                                alert("This user does not exist!");
                            }
                        })
                        
                }
            });
            var total_friends = [];
            var first_count_friend = 0;
            var second_count_friend = 0;
            firebase.database().ref("message_list/"+user_id+"/").once('value')
                .then(function (snapshot) {
                    //console.log(snapshot.key);
                    snapshot.forEach(function(childSnapshot) {
                        // key will be "Alan" the first time and "Jim" the second time
                        var childid = childSnapshot.key;
                        var childemail = childSnapshot.child('email');
                        console.log(childid, childemail.val());
                        total_friends[total_friends.length] = "<li class='contact' id='"+childid+"'><div class='wrap'><div class='meta'><p class='name'>"+childemail.val()+"</p></div></div></li>";
                        first_count_friend++;
                    })
                    friend_list.innerHTML = total_friends.join("");
                    firebase.database().ref("message_list/"+user_id+"/").on('child_added', function(data) {
                        second_count_friend++;
                        if(second_count_friend > first_count_friend){
                            var childid = data.key;
                            console.log(childid);
                            var childemail = data.child('email');
                            total_friends[total_friends.length] = "<li class='contact' id='"+childid+"'><div class='wrap'><div class='meta'><p class='name'>"+childemail.val()+"</p></div></div></li>";
                        }
                        friend_list.innerHTML = total_friends.join("");
                    });
                }).catch(e =>{ console.log(e.message)});

            message_list = document.getElementById("message_list");
            submit_btn = document.getElementById("submit_btn");
            input_text = document.getElementById("input_text");

            submit_btn.addEventListener("click", function(){
                if(input_text.value != "" && friend_email != ''){ 
                    var newcontent = firebase.database().ref("message_list/"+user_id+"/"+friend_id).push();
                    newcontent.set({
                        email: user_email,
                        data: input_text.value
                    });
                    newcontent = firebase.database().ref("message_list/"+friend_id+"/"+user_id).push();
                    newcontent.set({
                        email: user_email,
                        data: input_text.value
                    });
                    input_text.value = "";
                }
            });
            input_text.addEventListener("keypress", function(e){
                var key = e.which || e.keyCode;
                if (key === 13){
                    if(input_text.value != "" && friend_email != ''){ 
                        var newcontent = firebase.database().ref("message_list/"+user_id+"/"+friend_id).push();
                        newcontent.set({
                            email: user_email,
                            data: input_text.value
                        });
                        newcontent = firebase.database().ref("message_list/"+friend_id+"/"+user_id).push();
                        newcontent.set({
                            email: user_email,
                            data: input_text.value
                        });
                        input_text.value = "";
                    }
                }
            });

            var str_before_user = "<li class='sent'><p>";
            var str_after_user = "</p></li>";
            var str_before_friend = "<li class='reply'><p>";
            var str_after_friend = "</p></li>";
            
            

            friend_list.addEventListener("click",function(e) {
                if(e.target && e.target.nodeName == "LI") {
                    //console.log(e.target.id + " was clicked");
                        console.log(e.target);
                        friend_id = e.target.id;
                        console.log(friend_id);
                        var total_post = [];
                        var first_count = 0;
                        var second_count = 0;
                        firebase.database().ref("users/"+friend_id).once('value')
                            .then(function(snapshot){
                                friend_email = snapshot.val().email;
                                console.log(friend_id, friend_email);
                                current_friend.innerHTML = friend_email;
                                firebase.database().ref("message_list/"+user_id+"/"+friend_id).once('value')
                                    .then(function (snapshot) {
                                        //console.log(snapshot.key);
                                        snapshot.forEach(function(childSnapshot) {
                                            
                                            // key will be "Alan" the first time and "Jim" the second time
                                            var childkey = childSnapshot.key;
                                            var childData = childSnapshot.val();
                                            //console.log(childkey);
                                            if(childkey != "email"){
                                                if(childData.email == user_email) total_post[total_post.length] = str_before_user + childData.data + str_after_user;
                                                else 
                                                    total_post[total_post.length] = str_before_friend + childData.data + str_after_friend;
                                            }
                                            first_count++;
                                        });
                                        message_list.innerHTML = total_post.join("");
                
                                        firebase.database().ref("message_list/"+user_id+"/"+friend_id).on('child_added', function(data) {
                                            second_count++;
                                            if(second_count > first_count){
                                                var childData = data.val();
                                                if(childData.email == user_email) total_post[total_post.length] = str_before_user + childData.data + str_after_user;
                                                else total_post[total_post.length] = str_before_friend + childData.data + str_after_friend;
                                            }
                                            message_list.innerHTML = total_post.join("");
                                        });
                                    })
                                    .catch(e =>{ console.log(e.message)});
                        });
                    
                }
            });
            
        } else {
            // It won't show any post if not login
            //menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('username').innerHTML = "";
            document.getElementById('message_list').innerHTML = "";
            document.getElementById('contacts').innerHTML = "<ul></ul>";
        }
    });
    
    
}

window.onload = function () {
    try{init({ async: true });}catch(e){};
}