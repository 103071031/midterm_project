# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Chat Room
* Key functions (add/delete)
    1. 註冊、登入
    2. 加好友
    3. 聊天紀錄
    4. 與新使用者聊天
    
    
* Other functions (add/delete)
    1. Google登入 

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://chatroom-8e449.firebaseapp.com/

# Components Description : 
1. 註冊 : 輸入email. password並按下new account，用firebase.auth().createUserWithEmailAndPassword註冊使用者的身分，
2. 登入 : 輸入email. password並按下log in，用firebase.auth().signInWithEmailAndPassword登入，登入後進入到聊天頁面，並且將此user的email以及user id push到firebase database中
3. 加入好友 : 在搜尋欄填入其他已註冊之使用者的email，點擊下方add contact button後，好友就會出現在搜尋欄下方的好友列表中。加入好友後，右方聊天室的對象會自動更新為新加入的此位好友。每次加入好友，會在firebase的database建立一個屬於兩方的reference("message_list/user_id/friend_id")並將其value set成email，與此同時，另一個reference("message_list/friend_id/user_id")也會存放同一則訊息內容
4. 好友列表 : 
5. 聊天、聊天紀錄 : 每一則聊天內容皆會被push到firebase的database().ref("message_list/user_id/friend_id")去存放聊天訊息，與此同時，另一個ref("message_list/friend_id/user_id")也會存放同一則訊息內容，利用firebase.database().ref("message_list/user_id/friend_id").once讀取歷史訊息只要右方聊天室有對象(點擊好友列表後，上方會顯示聊天對象的email)，再利用firebase.database().ref("message_list/user_id/friend_id").on關注在聊天室中目前新增的聊天內容

# Other Functions Description(1~10%) : 
1. 使用google登入 : 用var provider = new firebase.auth.GoogleAuthProvider();  firebase.auth().signInWithPopup(provider)讓使用者也能用google帳戶註冊、登入、使用聊天室

## Security Report (Optional)
1. 網站預設為登入畫面，因此只要關閉網站頁面再重新開啟就必須重新登入
2. 在建立reference tree時，就讓使用者與朋友的聊天內容都有自己的tree，而朋友則另外建立與此使用者的tree，因此每個使用者都只會看見自己與朋友的聊天訊息，無法看見朋友與朋友的聊天內容。
